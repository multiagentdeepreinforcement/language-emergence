import pprint
import yaml
import argparse

pp = pprint.PrettyPrinter(indent=2)
arg_parser = argparse.ArgumentParser()
arg_parser.add_argument("file_name", help="File to test.")
args = arg_parser.parse_args()

config_path = "./{:s}.yaml".format(args.file_name)
f = open(config_path)
config_data = yaml.load(f, Loader=yaml.SafeLoader)
f.close()

pp.pprint(config_data)
