import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

from environment import Environment


def debug():
    args = {
        "agents_no": 2,
        "landmarks_no": 3,
        "batch_size": 1,
        "shuffle_colors": True,
        "view_other_agents": False,
        "goal_agent_by_color": True,
        "use_cuda": True,
        "gradients_through_env": True
    }

    def get_actions(inputs):
        x = inputs.view(args["batch_size"] * args["agents_no"], -1)
        actions = torch.ones(args["batch_size"] * args["agents_no"], 4) * 0.5
        p = nn.Parameter(torch.zeros(4, x.size(1)).cuda())
        return F.linear(x, p) + Variable(actions.cuda())


    e = Environment(args)
    print("Before:")
    (p, g), rewards = e.reset()
    # print(e.coords)
    # print(e.velocities)
    # print(e.rel_coords)
    # print(e._colors)
    # print(e._goal_agent_idx)
    # print(e.goals)
    (p, g), rewards = e.act(get_actions(p))
    print("After (1):")
    # print(e.coords)
    # print(e.velocities)
    (p, g), rewards = e.act(get_actions(p))
    print("After (2):")
    print("Rewards:")
    print(rewards)
    print("Coords:")
    print(e.coords)
    print(e.rel_coords)
    print("Colors")
    print(e._colors)
    print("Goal agent idx")
    print(e._goal_agent_idx)
    #print(e.velocities)
    #print(e.gaze)
    print("Goals:")
    print(e.goals)
    print(e.get_all_goals())

if __name__ == "__main__":
    debug()
