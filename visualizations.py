"""
Necessary package for displaying images
Install:: https://github.com/szym/display

Start server with:
    th -ldisplay.start 8000 0.0.0.0

Then open (link) in your browser to load the remote desktop
    http://localhost:8000/

"""
import torch
import numpy as np
import display
import cv2

from torch.nn.functional import pairwise_distance

MAP_SIZE_PIXEL = 256
BACKGROUND_COLOR = [200, 200, 200]

AGENT_CIRCLE_SIZE = 10
LANDMARK_CIRCLE_SIZE = 12
LANDMARK_RECT_SIZE = 6

GAZE_LENGTH = 50
LINE_GOTO_COLOR = (0,0,0)
LINE_GAZE_COLOR = (15,10,10)

FONT = cv2.FONT_ITALIC
FONT_SCALE = 0.5

class EnvVizualization(object):
    """
    Draws the environment

    """

    def __init__(self, env, args):

        self.agents_no = env.agents_no
        self.landmarks_no = env.landmarks_no
        self.batc_size = env.batch_size
        self.view_other_agents = env.view_other_agents
        self.goal_agent_by_color = env.goal_agent_by_color
        self.draw_agents_view = args.get("draw_agent_pov", True)
        self.draw_batch_no = args["draw_batch_no"]

        self.agent_size = AGENT_CIRCLE_SIZE
        self.landmark_size = LANDMARK_RECT_SIZE
        self.gaze_length = GAZE_LENGTH

        self._view_size = MAP_SIZE_PIXEL
        self._view_bkg = np.tile(BACKGROUND_COLOR, (self._view_size,
                                                    self._view_size, 1),
                                 ).astype(np.uint8)

        #Game step time (to display in windows)
        self.step_count = -1

        #Display portal for Full Map view
        self._view_win = display.image(self._view_bkg,
                                       title='Game:{}_step:{}'
                                       .format(self.draw_batch_no,
                                               self.step_count))

        self._view_agents = None

        self._view_size_agent = MAP_SIZE_PIXEL * 2
        self._view_bkg_agent = np.tile(BACKGROUND_COLOR, (self._view_size_agent,
                                                          self._view_size_agent,
                                                          1),
                                       ).astype(np.uint8)


    def _draw_landmark(self, img, pos, color):
        """Draw a landmark on a image"""
        color = tuple(map(int,color))
        pos_s = tuple(pos - self.landmark_size)
        pos_e = tuple(pos + self.landmark_size)
        cv2.rectangle(img, pos_s, pos_e, tuple(color), thickness=3)

    def _draw_agent(self, img, pos, gaze, color, goal_coord, goal_type,
                    velocity=None):
        """Draw agent on map (optional """
        color = tuple(map(int, color))
        pos = tuple(pos)
        gaze = tuple(gaze)
        goal_coord = tuple(goal_coord)
        agent_size = self.agent_size

        cv2.circle(img, pos, agent_size, color, thickness=2)
        cv2.line(img, pos, gaze, color, thickness=2)

        #GO TO goal
        if goal_type[0] == 1:
            cv2.line(img, pos, goal_coord, LINE_GOTO_COLOR, thickness=2)
        #Look at task
        elif goal_type[1] == 1:
            cv2.line(img, gaze, goal_coord, LINE_GAZE_COLOR, thickness=1)
        elif goal_type[2] == 1:
            cv2.circle(img, pos, 3, (5,5,5), thickness=2)

    def _draw_goals(self, img, goal_type, goal_source, goal_coord):
        goal_source = tuple(goal_source)
        goal_coord_ = tuple(goal_coord)
        cv2.circle(img, goal_source, 3, (5, 5, 5), thickness=1)
        pos_s = tuple(goal_coord - self.landmark_size)
        pos_e = tuple(goal_coord + self.landmark_size)
        cv2.rectangle(img, pos_s, pos_e, (5, 5, 5), thickness=3)

        #GO TO goal
        if goal_type[0] == 1:
            cv2.line(img, goal_source, goal_coord_, LINE_GOTO_COLOR, thickness=2)
        #Look at task
        elif goal_type[1] == 1:
            cv2.line(img, goal_source, goal_coord_, LINE_GAZE_COLOR, thickness=1)
        elif goal_type[2] == 1:
            cv2.circle(img, goal_source, 3, (5,5,5), thickness=2)

    def _draw_goal_no_position(self, img, goal_type, goal_color, goal_coord):
        cl = tuple(map(int, goal_color))
        cv2.rectangle(img, (0, 40), (40, 80), cl, thickness=-1)

        goal_coord = tuple(goal_coord)
        cv2.circle(img, goal_coord, LANDMARK_CIRCLE_SIZE, (5, 5, 5),
                   thickness=3)

        #GO TO goal
        if goal_type[0] == 1:
            cv2.putText(img, "GO TO", (40, 80), FONT, FONT_SCALE, (5, 5, 5))
        #Look at task
        elif goal_type[1] == 1:
            cv2.putText(img, "LOOK AT", (40, 80), FONT, FONT_SCALE, (5, 5, 5))
        elif goal_type[2] == 1:
            cv2.putText(img, "DO NOTHING", (40, 80), FONT, FONT_SCALE,(5, 5, 5))

    def init_env(self, goals, goal_landmark_idx, goal_agent_idx, _colors):
        _goals = goals.cpu()
        _goal_landmark_idx = goal_landmark_idx.cpu()
        if isinstance(goal_agent_idx, torch.autograd.variable.Variable):
            _goal_agent_idx = goal_agent_idx.data.cpu()
        else:
            _goal_agent_idx = goal_agent_idx.cpu()

        colors = _colors.cpu()
        gt = _goals[:, :, :3].clone().numpy()
        gl = _goal_landmark_idx.clone().numpy()
        self._goal_type = np.zeros(gt.shape)
        self._goal_l_idx = np.zeros(gl.shape)

        self._goal_agent_idx = _goal_agent_idx.clone().numpy()
        for b in range(self._goal_l_idx.shape[0]):
            self._goal_l_idx[b, self._goal_agent_idx[b]] = gl[b]
            self._goal_type[b, self._goal_agent_idx[b]] = gt[b]

        self._orig_colors = colors.clone()
        self._colors = torch.mul(colors.clone(), 255)


    def draw(self, coords, gaze, physical, goals, iteration):
        if isinstance(coords, torch.autograd.variable.Variable):
            _coords = coords.data.cpu()
            _gaze = gaze.data.cpu()
        else:
            _coords = coords.cpu()
            _gaze = gaze.cpu()
        self.draw_full_map(_coords, _gaze, iteration)
        if self.draw_agents_view:
            if isinstance(physical, torch.autograd.variable.Variable):
                _physical = physical.data.cpu()
                _goals = goals.data.cpu()
            else:
                _physical = physical.cpu()
                _goals = goals.cpu()
            self.draw_agents(_physical, _goals, iteration)

    def draw_full_map(self, _coords, _gaze, step):
        """Assumes following order - [agents, landmarks]"""
        draw_batch_no = self.draw_batch_no

        coords = _coords[draw_batch_no].clone()
        coords = torch.mul(coords, self._view_size)
        gaze = _gaze[draw_batch_no].clone()
        gaze = torch.mul(gaze, 255)
        goal_type = self._goal_type[draw_batch_no]
        goal_l_idx = self._goal_l_idx[draw_batch_no]

        coords = coords.int().numpy()
        gaze = gaze.int().numpy()

        colors = self._colors[draw_batch_no]
        colors = colors.int().numpy()

        envMap = self._view_bkg.copy()
        # DRAW landmarks
        for i in range(self.agents_no, len(coords)):
            self._draw_landmark(envMap, coords[i],
                                colors[i])
        # DRAW Agents
        for i in range(self.agents_no):
            self._draw_agent(envMap, coords[i], gaze[i], colors[i],
                             coords[int(goal_l_idx[i])], goal_type[i])

        # Draw map
        self._view_win = display.image(cv2.flip(envMap, 0),
                                       title='Game:{}_step:{}'.
                                       format(draw_batch_no, step),
                                       win=self._view_win)

    def draw_agents(self, _states, _goals, step):
        env_map = self._view_bkg.copy()
        draw_batch_no = self.draw_batch_no
        agents_no = self.agents_no
        view_size = self._view_size
        landmark_no = self.landmarks_no
        goal_agent_by_color = self.goal_agent_by_color

        states = _states.narrow(0, draw_batch_no * agents_no, agents_no).clone()
        goals = _goals.narrow(0, draw_batch_no * agents_no, agents_no).clone()

        view_size_port = self._view_size_agent /2
        coords = states.narrow(2,0,6)
        if self.view_other_agents:
            coord_size = torch.Size((agents_no, (agents_no + landmark_no),3,2))
        else:
            coord_size = torch.Size((agents_no, (1 + landmark_no),3,2))
        coords.mul_(view_size_port).add_(view_size_port).int()
        coords = coords.contiguous().view(coord_size).numpy()
        colors = states.narrow(2,6,3)
        colors = colors.mul_(255).int().numpy()
        agents_color = self._colors[draw_batch_no]
        agents_color = agents_color.int().numpy()
        orig_color = self._orig_colors[draw_batch_no].numpy()

        goals_type = goals.narrow(1,0,3).numpy()
        goals_coord = goals.narrow(1,3,2)
        goals_coord.mul_(view_size_port).add_(view_size_port)
        goals_coord = goals_coord.int().numpy()
        goals_color = None

        if not goal_agent_by_color:
            goals_source = goals[:,5:]
            goals_source.mul_(view_size_port).add_(view_size_port)
            goals_source = goals_source.int().numpy()
        else:
            goals_color = goals[:,5:]
            goals_color.mul_(255)
            goals_color = goals_color.int().numpy()
            pass

        agents_ix = list()
        land_mark_idx = list()

        if self.view_other_agents:
            agents_ix = range(agents_no)
            land_mark_idx = range(agents_no, agents_no + landmark_no)
        else:
            agents_ix = [0]
            land_mark_idx = range(1, 1 + landmark_no)

        full_window = np.zeros((self._view_size_agent, 0, 3))
        pos_text = (int(0), int(20))
        for agent in range(self.agents_no):
            cd = coords[agent]
            cl = colors[agent]
            env_map = self._view_bkg_agent.copy()

            for i in land_mark_idx:
                self._draw_landmark(env_map, cd[i, 0], cl[i])
            for i in agents_ix:
                self._draw_agent(env_map, cd[i, 0], cd[i, 2], cl[i],
                                 (0, 0), [0, 0, 0])
            if goal_agent_by_color:
                self._draw_goal_no_position(env_map, goals_type[agent],
                                            goals_color[agent],
                                            goals_coord[agent])
            else:
                self._draw_goals(env_map, goals_type[agent], goals_source[agent],
                                 goals_coord[agent])

            ag_col = tuple(map(int, agents_color[agent]))
            cv2.putText(env_map, "Agent{}::({},{},{})".format(agent,
                                                             *orig_color[agent]),
                        pos_text, FONT, FONT_SCALE,
                        ag_col)

            full_window = np.concatenate((full_window, env_map), axis=1)
            full_window = np.concatenate((full_window, np.zeros(
                (self._view_size_agent, 20, 3))), axis=1)

        self._view_agents = display.image(cv2.flip(full_window, 0),
                                          title='Agents.Game:{}_step:{}'.
                                          format(draw_batch_no, step),
                                          win=self._view_agents)
