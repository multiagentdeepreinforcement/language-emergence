import torch

from torch.nn.functional import pairwise_distance as batch_dist
from torch.autograd import Variable
from torch import Size as Size


def _generate_colors_on_sphere(n):
    from itertools import product
    import math

    n1 = int(math.ceil(math.sqrt(float(n))))
    step = 2 * math.pi / n1

    angles = [(d1*step, d2*step) for (d1, d2) in product(range(n1), range(n1))]

    cartesian = [(0.5 + 0.5 * math.sin(theta) * math.cos(phi),
                  0.5 + 0.5 * math.sin(theta) * math.sin(phi),
                  0.5 + 0.5 * math.cos(phi)) for (theta, phi) in angles]

    return torch.Tensor(cartesian[:n])

def generate_colors(n):
    if n <= 6:
        # Maybe 8 to include .0,.0,.0 and 1.,1.,1.?
        return torch.Tensor([
            [.0, .0, 1.], [.0, 1., .0], [1., .0, .0],
            [.0, 1., 1.], [1., .0, 1.], [1., 1., .0]
        ][:n])
    else:
        return _generate_colors_on_sphere(n)

# -----------------------------------------
# Environment implementation


class Environment(object):
    """This class implements the environment from [needs citation]."""



    def __init__(self, args):
        """Initializes needed tensors for @batch_size environments

        args:
         - "agents_no"
         - "landmarks_no"
         - "batch_size"
         - "shuffle_colors"
         - "view_other_agents"
         - "goal_agent_by_color"
         - "use_cuda"
         - "gradients_through_env"
        """

        self.agents_no = agents_no = args["agents_no"]
        self.landmarks_no = landmarks_no = args["landmarks_no"]
        self.entities_no = entities_no = agents_no + landmarks_no

        self.batch_size = batch_size = args["batch_size"]
        self.shuffle_colors = shuffle_colors = args.get("shuffle_colors", True)
        self.view_other_agents = view_other_agents = args["view_other_agents"]
        self.goal_agent_by_color = args.get("goal_agent_by_color", True)
        self.use_cuda = use_cuda = args["use_cuda"]

        visible_no = entities_no if view_other_agents else landmarks_no
        self.visible_no = visible_no

        self.Tensor = torch.cuda.FloatTensor if use_cuda else torch.FloatTensor

        # Tensors' shapes: coordinates, velocity, gaze

        self.abs_coords_size = Size((batch_size, entities_no, 2))
        self.rel_en_coords_size = Size((batch_size, agents_no, entities_no, 2))
        self.rel_ag_coords_size = Size((batch_size, agents_no, agents_no, 2))
        self.rel_lm_coords_size = Size((batch_size, agents_no, landmarks_no, 2))
        self.own_coords_size = Size((batch_size, agents_no, 2))

        if not view_other_agents:
            # Initialize tensor for own coordinates as they are always (0, 0)
            own_coords = self.Tensor(self.own_coords_size).zero_()
            self.own_coords = Variable(own_coords)

        lm_padding = self.Tensor(self.rel_lm_coords_size).zero_()
        self.lm_padding = Variable(lm_padding)

        # Tensors' shapes: colors

        self.all_colors_size = Size((batch_size, entities_no, 3))
        self.rel_en_colors_size = Size((batch_size, agents_no, entities_no, 3))
        self.rel_lm_colors_size = Size((batch_size, agents_no, landmarks_no, 3))
        self.rel_ag_colors_size = Size((batch_size, agents_no, agents_no, 3))
        self.own_colors_size = Size((batch_size, agents_no, 3))

        self.color_set = generate_colors(entities_no)
        self._colors = self.Tensor(self.all_colors_size)

        # Prepare tensors for goals

        if self.goal_agent_by_color:
            self.goals_size = Size((batch_size, agents_no, 3 + 2 + 3))
        else:
            self.goals_size = Size((batch_size, agents_no, 3 + 2 + 2))

        self._goals = torch.Tensor(self.goals_size)
        self._goal_type = torch.LongTensor(batch_size, agents_no)
        self._goal_agent_idx = torch.LongTensor(batch_size, agents_no)
        self._goal_landmark_idx = torch.LongTensor(batch_size, agents_no)
        if use_cuda:
            self._goals = self._goals.cuda()
            self._goal_type = self._goal_type.cuda()
            self._goal_agent_idx = self._goal_agent_idx.cuda()
            self._goal_landmark_idx = self._goal_landmark_idx.cuda()

        # Others
        self.dampening = 0.5 # dampening
        self.time_step = 0.3 # simulation time step

    @property
    def phyisical_size(self):
        a = self.agents_no if self.view_other_agents else 1
        return (a + self.landmarks_no) * (2 + 2 + 2 + 3)

    @property
    def goal_size(self):
        return self.goals_size[2]

    def reset(self):
        """Resets all environments"""

        agents_no = self.agents_no
        landmarks_no = self.landmarks_no
        entities_no = agents_no + landmarks_no
        batch_size = self.batch_size
        view_other_agents = self.view_other_agents
        use_cuda = self.use_cuda

        # Reset coords, velocities, and gazes

        coords = torch.Tensor(self.abs_coords_size).uniform_()
        velocities = torch.Tensor(self.own_coords_size).zero_()
        gaze = torch.Tensor(self.own_coords_size).uniform_()

        if use_cuda:
            coords = coords.cuda()
            velocities = velocities.cuda()
            gaze = gaze.cuda()

        self.coords = Variable(coords, requires_grad=False)

        self.velocities = Variable(velocities, requires_grad=False)
        self.gaze = Variable(gaze, requires_grad=False)

        self._compute_relative()

        # Reset colors and shapes

        color_set = self.color_set
        _colors = self._colors
        if not self.shuffle_colors:
                _colors.copy(color_set.unsqueeze(0).expand(self.all_colors_size))
        else:
            for b in range(batch_size):
                c = color_set.index_select(0, torch.randperm(entities_no))
                _colors[b].copy_(c)

        rel_en_colors_size = self.rel_en_colors_size
        rel_en_colors = _colors.unsqueeze(1).expand(rel_en_colors_size)
        self.rel_en_colors = rel_en_colors

        if view_other_agents:
            self.rel_colors = Variable(self.rel_en_colors)
        else:
            own_colors = _colors.narrow(1, 0, agents_no).unsqueeze(2)
            rel_lm_colors = rel_en_colors.narrow(2, agents_no, landmarks_no)
            self.rel_colors = Variable(torch.cat([own_colors,rel_lm_colors], 2))

        # Goals (type, landmark coords, agent)

        goals = self._goals
        goal_type = self._goal_type
        goal_agent_idx = self._goal_agent_idx
        goal_landmark_idx = self._goal_landmark_idx
        goals.zero_()
        if use_cuda:
            goal_type.copy_(torch.LongTensor(goal_type.size()).random_(0, 2))
        else:
            goal_type.random_(0, 2)
        goals[:,:,:3].scatter_(2, goal_type.unsqueeze(2), 1)


        if use_cuda:
            goal_landmark_idx.copy_(
                torch.LongTensor(goal_landmark_idx.size()) \
                     .random_(agents_no, entities_no - 1)
                )
        else:
            goal_landmark_idx.random_(agents_no, entities_no - 1)

        for b in range(batch_size):
            b_a_idx = goal_agent_idx[b]
            if use_cuda:
                # No conflicts for now
                b_a_idx.copy_(torch.randperm(agents_no))
            else:
                torch.randperm(agents_no, out=b_a_idx)
            if self.goal_agent_by_color:
                _colors[b].index_select(0, b_a_idx, out=goals[b, :, 5:])

        self.goal_agent_idx = Variable(goal_agent_idx)

        self._adjust_goals()

        return self._observe(), None

    def _compute_relative(self):
        agents_no = self.agents_no
        landmarks_no = self.landmarks_no
        view_other_agents = self.view_other_agents

        coords = self.coords
        self.ag_coords = self.coords.narrow(1, 0, agents_no)
        self.lm_coords = self.coords.narrow(1, agents_no, landmarks_no)
        ag_coords, lm_coords = self.ag_coords, self.lm_coords

        rel_en_coords_size = self.rel_en_coords_size
        rel_coords = coords.unsqueeze(1).expand(rel_en_coords_size) - \
                     ag_coords.unsqueeze(2).expand(rel_en_coords_size)
        self.rel_coords = rel_coords

        self.rel_lm_coords = rel_coords.narrow(2, agents_no, landmarks_no)
        assert self.rel_lm_coords.size() == self.rel_lm_coords_size

        gaze = self.gaze

        rel_ag_coords_size = self.rel_ag_coords_size
        self.rel_gaze = gaze.unsqueeze(1).expand(rel_ag_coords_size) -\
                        ag_coords.unsqueeze(2).expand(rel_ag_coords_size)

        if not view_other_agents:
            self.own_gaze = gaze - ag_coords


    def _adjust_goals(self):
        """Put back relative landmark position and agent position"""

        agents_no = self.agents_no
        landmarks_no = self.landmarks_no
        entities_no = self.entities_no
        batch_size = self.batch_size
        view_other_agents = self.view_other_agents

        coords = self.coords.data
        rel_coords = self.rel_coords.data

        goals = self._goals
        goal_agent_idx = self._goal_agent_idx
        goal_landmark_idx = self._goal_landmark_idx

        # Refresh relative goal landmark coordinates
        _sz = torch.Size((batch_size, agents_no, 1, 2))
        l_idx = goal_landmark_idx.unsqueeze(2).unsqueeze(2).expand(_sz)
        goal_coords = rel_coords.gather(2, l_idx)
        goals[:,:,3:5] = goal_coords
        self.goal_coords = Variable(goal_coords)

        # Refresh relative goal agent coordinates
        if not self.goal_agent_by_color:
            a_idx = goal_agent_idx.unsqueeze(2).unsqueeze(2).expand(_sz)
            goals[:,:,5:7] = rel_coords.gather(2, a_idx)

        # Create variable
        self.goals = Variable(goals.view(batch_size * agents_no, -1))


    def _physical_perception(self):
        """Compose physical perception for all agents"""

        agents_no, landmarks_no = self.agents_no, self.landmarks_no
        entities_no = agents_no + landmarks_no
        batch_size = self.batch_size
        view_other_agents = self.view_other_agents

        features = []

        if view_other_agents:
            features.append(self.rel_coords)
        else:
            own_coords = self.own_coords.unsqueeze(2)
            features.append(torch.cat([own_coords, self.rel_lm_coords], 2))

        lm_padding = self.lm_padding

        if view_other_agents:
            _size = self.rel_ag_coords_size
            s_velocities = self.velocities.unsqueeze(1).expand(_size)
        else:
            s_velocities = self.velocities.unsqueeze(2)

        features.append(torch.cat([s_velocities, lm_padding], 2))

        if view_other_agents:
            s_gaze = self.rel_gaze
        else:
            s_gaze = self.own_gaze.unsqueeze(2)

        features.append(torch.cat([s_gaze, lm_padding], 2))

        features.append(self.rel_colors)

        if view_other_agents:
            output_size = Size((batch_size * agents_no, entities_no, -1))
        else:
            output_size = Size((batch_size * agents_no, landmarks_no + 1, -1))
        prepare = lambda t: t.contiguous().view(output_size)

        return torch.cat([prepare(t) for t in features], 2)

    def _observe(self):
        return (self._physical_perception(), self.goals)

    def _change_state(self, actions):
        """Applies actions in all environments"""

        agents_no, landmarks_no = self.agents_no, self.landmarks_no
        batch_size =  self.batch_size
        assert(actions.size() == Size((batch_size * agents_no, 4)))
        actions = actions.view(batch_size, agents_no, 4)

        time_step, dampening = self.time_step, self.dampening
        coords, velocities = self.coords, self.velocities

        acceleration = actions.narrow(2, 0, 2)
        focus = actions.narrow(2, 2, 2)

        # Update coordinates
        new_coords = torch.cat([
            coords.narrow(1, 0, agents_no) + velocities * time_step,
            coords.narrow(1, agents_no, landmarks_no)
        ], 1)
        self.coords = new_coords.clamp(.0, 1.)

        # Update velocities
        new_velocities = velocities * dampening + acceleration * time_step
        self.velocities = new_velocities

        # Update gaze
        self.gaze = coords.narrow(1, 0, agents_no) + focus

        self._compute_relative()
        self._adjust_goals()

        # TODO: collisions

    def _compute_rewards(self):
        """Computes rewards for current state

        For each agent:
         - gets its goal agent idx
         - gets its goal landmark idx
         - gets the coordinates of the goal landmark in the goal agent's view
         - the reward is the distance to the landmark (if goal type is "GO")
         - or the reward is the distance between the gaze and that landmark
        """

        agents_no = self.agents_no
        batch_size = self.batch_size

        rel_coords = self.rel_coords
        rel_gaze = self.rel_gaze

        g_ag_idx = self.goal_agent_idx.unsqueeze(2) \
                                       .unsqueeze(2) \
                                       .expand(batch_size, agents_no, 1, 2)
        g_ag_pos = rel_coords.gather(2, g_ag_idx).view(-1, 2)
        g_ag_gaze = rel_gaze.gather(2, g_ag_idx).view(-1, 2)

        goal_coords = self.goal_coords.view(-1, 2)
        goals = self.goals.view(batch_size * agents_no, -1)

        return torch.mul(batch_dist(g_ag_pos, goal_coords), goals.select(1,0)) \
            +  torch.mul(batch_dist(g_ag_gaze, goal_coords), goals.select(1,1))

    def act(self,actions):
        self._change_state(actions)
        return self._observe(), self._compute_rewards()


    def get_all_goals(self):
        batch_size = self.batch_size
        agents_no = self.agents_no
        landmarks_no = self.landmarks_no

        all_g = self._goals.new().resize_(batch_size, agents_no, 9)

        goal_agent_idx = self._goal_agent_idx
        goal_landmark_idx = self._goal_landmark_idx

        colors = self._colors

        all_g[:, :, :3].copy_(self._goals[:, :,:3])

        for b in range(batch_size):
            b_a_idx = goal_agent_idx[b]
            b_l_idx = goal_landmark_idx[b]
            all_g[b, :, 3:6] = colors[b].index_select(0, b_l_idx)
            all_g[b, :, 6:9] = colors[b].index_select(0, b_a_idx)

        all_g = all_g.unsqueeze(1).expand(batch_size, agents_no, agents_no, 9) \
                                  .contiguous()
        return Variable(all_g.view(batch_size * agents_no, agents_no, 9))


if __name__ == "__main__":

    import torch.nn as nn
    import torch.nn.functional as F

    class TestPolicy(nn.Module):
        def __init__(self, p_size, g_size):
            super(TestPolicy, self).__init__()
            self.p_size, self.g_size = p_size, g_size
            self.linear = nn.Linear(p_size + g_size, 4)

        def forward(self, p, g):
            p_size, g_size = self.p_size, self.g_size
            inputs = torch.cat([physical.view(-1, p_size), goals], 1)
            return F.tanh(self.linear(inputs))

    class GoalPrediction(nn.Module):
        def __init__(self, p_size, g_size, agents_no):
            super(GoalPrediction, self).__init__()
            self.agents_no = agents_no
            self.p_size, self.g_size = p_size, g_size
            self.linear = nn.Linear(p_size + g_size, agents_no * 9)

        def forward(self, p, g):
            agents_no = self.agents_no
            p_size, g_size = self.p_size, self.g_size
            inputs = torch.cat([physical.view(-1, p_size), goals], 1)
            y = self.linear(inputs)
            y1 = F.softmax(y.narrow(1, 0, 3 * agents_no))
            y2 = F.sigmoid(y.narrow(1, 3 * agents_no, 9 * agents_no))
            return torch.cat([y1, y2], 1)

    test_args = {
        "agents_no": 10,
        "landmarks_no": 9,
        "batch_size": 1024,
        "steps_no": 20,
        "shuffle_colors": False,
        "view_other_agents": False,
        "goal_agent_by_color": False,
        "use_cuda": True,
        "iterations_no": 10
    }
    test_env = Environment(test_args)
    test_policy = TestPolicy(test_env.phyisical_size, test_env.goal_size)
    test_goal = GoalPrediction(test_env.phyisical_size, test_env.goal_size,
                               test_args["agents_no"])
    if test_args["use_cuda"]:
        test_policy.cuda()
        test_goal.cuda()

    for i in range(test_args["iterations_no"]):
        test_policy.zero_grad()
        (physical, goals), _ = test_env.reset()
        all_rewards = []
        for step in range(test_args["steps_no"]):
            actions = test_policy(physical, goals)
            (physical, goals), rewards = test_env.act(actions)
            all_rewards.append(rewards)
        targets = test_env.get_all_goals()
        pred = test_goal(physical, goals)
        print(targets.size())
        print(pred.size())
        loss = nn.BCELoss()(pred, targets)
        s = torch.sum(torch.cat(rewards, 0)) + loss
        print(s)
        s.backward(s.data.clone().fill_(1.0))
