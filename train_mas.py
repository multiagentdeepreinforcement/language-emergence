import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

from environment import Environment
from models import get_model

from util import ModelUtil
from util import AverageMeter

import time
from termcolor import colored as clr
from argparse import Namespace
import select
import sys


def get_configuration():
    import yaml
    import argparse

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("-cf" "--config_file",
                            default="basic", dest="config_file",
                            help="Configuration file.")
    args = arg_parser.parse_args()
    with open("./configs/{:s}.yaml".format(args.config_file)) as f:
        config_data = yaml.load(f, Loader=yaml.SafeLoader)

    return config_data

def print_info(info):
    sep = clr(" | ", "white")
    header = clr("-------------------------------------------", "blue")
    first_line = clr("Iteration {:5d}".format(info.i), "yellow")

    if info.prev_best_reward < info.last_reward:
        last_reward = clr("{:6.4f}".format(info.last_reward), "white", "on_red")
        best_reward = clr("{:6.4f}".format(info.best_reward), "white", "on_red")
    else:
        last_reward = clr("{:6.4f}".format(info.last_reward), "magenta")
        best_reward = clr("{:6.4f}".format(info.best_reward), "magenta")

    first_line += sep + "Last reward: {:s}".format(last_reward) + sep + \
                  "Best reward: {:s}".format(best_reward)

    second_line = "Max param: {:s}".format(
        clr("{:4.6f}".format(info.max_param), "green")
    )
    second_line += sep + "Avg param: {:s}".format(
        clr("{:4.6f}".format(info.avg_param), "green")
    )

    second_line += sep + "Max grad: {:s}".format(
        clr("{:4.6f}".format(info.max_grad), "green")
    )
    second_line += sep + "Avg grad: {:s}".format(
        clr("{:4.6f}".format(info.avg_grad), "green")
    )

    rewards_line = "R: " + \
                   clr("{:4.6f}".format(info.R), "yellow") + \
                   sep + \
                   "Rg: " + \
                   clr("{:4.6f}".format(info.Rg), "yellow") + \
                   sep + \
                   "Ra: " + \
                   clr("{:4.6f}".format(info.Ra), "yellow") + \
                   sep + \
                   "Rc: " + \
                   clr("{:4.6f}".format(info.Rc), "yellow") + \
                   sep + \
                   "Rd: " + \
                   clr("{:4.6f}".format(info.Rd), "yellow")

    progress_line = "diff (RT-R0): "
    if info.RT < info.R0:
        progress_line += clr("{:4.6f}".format(info.RT - info.R0), "red")
    else:
        progress_line += clr("{:4.6f}".format(info.RT - info.R0),
                             "red", "on_white")

    time_line = "Time: " + \
                clr("{:2.4f}".format(info.time_step.val), "yellow") + \
                sep + \
                "Avg. time: " + \
                clr("{:2.4f} s".format(info.time_step.avg), "yellow")
    print(header)
    print(first_line)
    print(second_line)
    print(rewards_line)
    print(progress_line)
    print(time_line)


if __name__ == "__main__":
    config = get_configuration()

    general_config = config["general"]
    env_config = config["environment"]
    model_config = config["model"]
    training_config = config["training"]
    visualization_config = config["visualization"]

    use_cuda = general_config["use_cuda"]
    v_size = general_config["vocabulary_size"]
    batch_size = general_config["batch_size"]
    use_communication = general_config["use_communication"]
    step_time = AverageMeter()

    env = Environment(env_config)
    agents_no = env.agents_no
    full_batch_size = batch_size * agents_no

    Model = get_model(model_config["name"])
    pi = Model(model_config)

    # Model saver & loader
    model_util = ModelUtil(pi, model_config)
    if model_config["load_model_path"]:
        model_util.loadModelFromFile(model_config["load_model_path"])
    model_save_freq = model_config["save_model_freq"]

    if use_cuda:
        pi.cuda()

    Optimizer = getattr(optim, training_config["optimizer"])
    optimizer = Optimizer(pi.parameters(), **training_config["optimizer_args"])

    iterations_no = training_config["iterations_no"]
    episode_length = training_config["episode_length"]
    c = training_config["coefficients"]

    draw_game = visualization_config["draw_game"]
    draw_iteration = False
    if draw_game:
        from visualizations import EnvVizualization
        env_draw = EnvVizualization(env, visualization_config)

    train_rewards = torch.zeros(iterations_no)

    nk = torch.zeros(v_size)
    nk = nk.cuda() if use_cuda else nk

    if use_communication:
        m_size = torch.Size((full_batch_size, agents_no, v_size))
        zero_messages = Variable(torch.zeros(m_size))
        if use_cuda:
            zero_messages = zero_messages.cuda()

    __ones = torch.ones(full_batch_size)
    __ones = __ones.cuda() if use_cuda else __ones

    info = Namespace()
    info.time_step = step_time
    for i in range(iterations_no):
        start_step_time = time.time()

        info.i = i
        nk.zero_()
        optimizer.zero_grad()

        (physical, goals), _ = env.reset()
        messages = zero_messages
        hidden_states = None

        all_rewards = []

        info.Ra = .0
        info.Rc = .0
        info.Rd = .0
        info.R = .0
        info.R0 = .0
        info.Rt = .0

        if draw_game:
            if sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
                #Command from input (Press Enter)
                #Init drawing environment
                draw_iteration = True
                env_draw.init_env(env._goals, env._goal_landmark_idx,
                                  env.goal_agent_idx, env._colors)
                env_draw.draw(env.coords, env.gaze, physical, goals, -1)
                input(); input()

        for t in range(episode_length):
            states = (physical, goals, messages)

            actions, others_goals, messages, hidden_states =\
                        pi(states, hidden_states,
                           predict_goals=(t == episode_length))

            Ra = -torch.sum(actions * actions)
            all_rewards.append(torch.sum(actions * actions) * c["norm_u"])
            info.Ra += Ra.data[0]

            if use_communication:
                Rc = -torch.sum(messages * messages)
                all_rewards.append(Rc * c["norm_c"])
                info.Rc += Rc.data[0]

                _, idx = messages.data.max(1)
                dirichlet = torch.log(nk / (nk.sum() + 1) +1e-9) \
                                 .unsqueeze(0) \
                                 .expand(full_batch_size, v_size)

                Rd = torch.mul(messages, Variable(dirichlet)).sum()
                all_rewards.append(Rd * c["rd"])
                info.Rd += Rd.data[0]

                messages = messages.view(batch_size, agents_no, v_size)\
                            .unsqueeze(1)\
                            .expand(batch_size, agents_no, agents_no, v_size)\
                            .contiguous()\
                            .view(full_batch_size, agents_no, v_size)
                nk.index_add_(0, idx.view(-1), __ones)


            (physical, goals), rewards = env.act(actions)

            R = -torch.sum(rewards)
            info.R += R.data[0]
            all_rewards.append(R * c["goal"])
            if t == 0:
                info.R0 = R.data[0]
            elif t == episode_length - 1:
                info.RT = R.data[0]

            if draw_iteration:
                env_draw.draw(env.coords, env.gaze, physical, goals, t)
                input()


        targets = env.get_all_goals()
        Rg = -nn.BCELoss()(others_goals, targets)
        all_rewards.append(Rg * c["other"])
        info.Rg = Rg.data[0]

        total_loss = torch.sum( torch.cat(all_rewards,0))

        train_rewards[i] = -total_loss.data[0]
        total_loss.backward(None)

        param_avg, grad_avg = .0, .0
        param_max, grad_max = None, None
        param_groups_count = .0
        for p in pi.parameters():
            param_avg += p.data.abs().mean()
            grad_avg += p.grad.data.abs().mean()
            p_max = p.data.abs().max()
            g_max = p.grad.data.abs().max()
            param_max = max(p_max, param_max) if param_max else p_max
            grad_max = max(g_max, param_max) if param_max else g_max
            param_groups_count += 1

        param_avg = param_avg / param_groups_count
        grad_avg = grad_avg / param_groups_count

        info.avg_param = param_avg
        info.max_param = param_max
        info.avg_grad = grad_avg
        info.max_grad = grad_max

        optimizer.step()
        step_time.update(time.time() - start_step_time)

        #SAVE MODEL
        if (i + 1) % model_save_freq == 0:
            model_util.save_model(total_loss.data[0], i)

        info.last_reward = -total_loss.data[0]
        if not hasattr(info, "best_reward"):
            info.best_reward = info.last_reward - 1
        info.prev_best_reward = info.best_reward
        info.best_reward = max(info.best_reward, info.last_reward)

        if draw_game:
            draw_iteration = False

        print_info(info)
