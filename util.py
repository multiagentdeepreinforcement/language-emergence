import numpy as np
import torch
import os
import os.path as path

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

class ModelUtil(object):
    min_reward = np.inf

    def __init__(self, model, config):
        self.save_path = path.join(config["save_folder"], config["save_prefix"])
        self.arch_name = config["name"]
        self.model = model

    def save_model(self, reward, iteration, save_only_min=True):
        save_path = self.save_path
        model = self.model
        if not save_only_min:
            torch.save({
                'iteration': iteration,
                'arch': self.arch_name,
                'state_dict': model.state_dict(),
                'reward': reward,
                'min_reward': self.min_reward
            }, (save_path + "_{}".format(iteration)))

        if reward < self.min_reward:
            torch.save({
                'iteration': iteration,
                'arch': self.arch_name,
                'state_dict': model.state_dict(),
                'reward': reward,
                'min_reward': self.min_reward
            }, (save_path + "_min_reward"))
            self.min_reward = reward


    def loadModelFromFile(self, path):
        model = self.model

        if os.path.isfile(path):
            print("=> loading checkpoint '{}'".format(path))
            checkpoint = torch.load(path)
            iteration = checkpoint['iteration']
            reward = checkpoint['reward']
            model.load_state_dict(checkpoint['state_dict'])
            print("=> loaded checkpoint '{}' (iteration {} -- reward: {})"
                             .format(path, iteration, reward))
        else:
            print("=> no checkpoint found at '{}'".format(path))
