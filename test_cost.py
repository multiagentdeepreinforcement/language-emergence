import torch
import torch.nn as nn
import torch.nn.functional as F

import torch.optim as optim
from torch.autograd import Variable

BATCH_SIZE = 1024
INPUTS = 10
VOC_SIZE = 10

class Gumbel(nn.Module):
    def __init__(self):
        super(Gumbel, self).__init__()

    def forward(self, p, tau=2.0):
        noise = torch.rand(p.size()).type_as(p.data)
        noise.add_(1e-9).log_().neg_()
        noise.add_(1e-9).log_().neg_()
        noise = Variable(noise)
        x = (p + noise) / tau
        x = F.softmax(x.view(-1, p.size(1)))
        return x.view_as(p)

model = nn.Sequential(
    nn.Linear(INPUTS, INPUTS),
    nn.Linear(INPUTS, VOC_SIZE),
    Gumbel()
)

model.cuda()
optimizer = optim.Adam(model.parameters())

nk = torch.zeros(VOC_SIZE).cuda()
nk2 = torch.zeros(VOC_SIZE).cuda()

for i in range(200000):
    optimizer.zero_grad()
    x = Variable(torch.randn(BATCH_SIZE, INPUTS).cuda())
    y = model(x)
    _, idx = y.data.max(1)

    t = torch.mul(torch.log(nk / (nk.sum()+10) +1e-9),
                  nk.clone().fill_(1)).unsqueeze(0) \
                      .expand(BATCH_SIZE, VOC_SIZE)
    #r = torch.zeros(BATCH_SIZE, VOC_SIZE).scatter_(1, idx, t.gather(1,idx))
    y.backward(-t)
    optimizer.step()
    # print(nk)
    nk.index_add_(0, idx.view(-1), torch.ones(BATCH_SIZE).cuda())
    nk2.zero_().index_add_(0, idx.view(-1), torch.ones(BATCH_SIZE).cuda())
    if i % 1000 == 0:
        print(nk)
        print(nk2)
        print(y[0])
print(nk)
