import torch
import torch.nn as nn
import torch.nn.functional as F

from torch.autograd import Variable
from torch import Size as Size

class DummyModule(nn.Module):

    def __init__(self, args):
        super(DummyModule, self).__init__()

        self.agents_no = agents_no = args["agents_no"]
        self.landmarks_no = landmarks_no = args["landmarks_no"]
        self.entities_no = entities_no = agents_no + landmarks_no
        self.vocabulary_size = vocabulary_size = args["vocabulary_size"]
        self.batch_size = batch_size = args["batch_size"]
        self.view_other_agents = view_other_agents = args["view_other_agents"]
        self.goal_agent_by_color = args["goal_agent_by_color"]
        self.use_communication = use_communication = args["use_communication"]

        self.visible_no = entities_no if view_other_agents else (landmarks_no+1)
        self.p_features_no = p_features_no = 2 + 2 + 2 + 3

        self.p_out = p_out = 32

        self.fcx_l1 = nn.Conv1d(p_features_no, p_out, 1)
        self.fcx_l2 = nn.MaxPool1d(self.visible_no)

        if self.goal_agent_by_color:
            self.g_size = g_size = 3 + 2 + 3
        else:
            self.g_size = g_size = 3 + 2 + 2

        in_size = p_out + g_size

        if use_communication:
            self.c_out = c_out = 32
            self.fcc_l1 = nn.Conv1d(self.vocabulary_size, c_out, 1)
            self.fcc_l2 = nn.MaxPool1d(self.agents_no)

            in_size += c_out

        self.in_size = in_size
        self.hidden_size = hidden_size = 32
        self.out_size = out_size = 4

        self.hidden = nn.Linear(in_size + hidden_size, hidden_size)
        self.policy = nn.Linear(hidden_size, out_size)

        if use_communication:
            self.speak = nn.Linear(hidden_size, vocabulary_size + 1)

        self.guess = nn.Linear(hidden_size, g_size * agents_no)

    def forward(self, states, hidden_state=None, predict_goals=False):

        agents_no = self.agents_no
        batch_size = self.batch_size
        vocabulary_size = self.vocabulary_size
        p_features_no = self.p_features_no
        visible_no = self.visible_no
        g_size = self.g_size
        N = agents_no * batch_size

        if self.use_communication:
            physical, goal, verbal = states
            assert verbal.size() == Size((N, vocabulary_size, agents_no))
        else:
            physical, goal = states
        assert physical.size() == Size((N, p_features_no, visible_no))
        assert goal.size() == Size((N, g_size))

        if hidden_state is None:
            hidden_state = Variable(torch.zeros(N, self.hidden_size))

        # Process physical
        p_out = F.elu(self.fcx_l2(F.elu(self.fcx_l1(physical))))

        if self.use_communication:
            c_out = F.elu(self.fcc_l2(F.elu(self.fcc_l1(verbal))))

        inputs = [p_out.view(N, -1), goal, hidden_state]
        if self.use_communication:
            inputs.append(c_out.view(N, -1))

        inputs = torch.cat(inputs, 1)
        print(inputs.size())
        hidden_state = F.tanh(self.hidden(inputs))

        actions = F.tanh(self.policy(hidden_state))
        if predict_goals:
            others_goals = F.sigmoid(self.guess(hidden_state))
        else:
            others_goals = None
        if self.use_communication:
            messages = F.softmax(self.speak(hidden_state))
        else:
            messages = None

        return actions, others_goals, messages, hidden_state
