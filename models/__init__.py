from .dummy_module import DummyModule
from .communication_module import CommunicationModule

ALL_MODELS = {
    "dummy": DummyModule,
    "original": CommunicationModule
}

def get_model(model_name):
    return ALL_MODELS[model_name]
