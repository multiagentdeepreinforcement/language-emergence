import torch
import torch.nn as nn
import torch.nn.functional as F

from torch.autograd import Variable
from torch import Size as Size

def gumbel(p, tau=1.0):
    noise = torch.rand(p.size()).type_as(p.data)
    noise.add_(1e-9).log_().neg_()
    noise.add_(1e-9).log_().neg_()
    noise = Variable(noise)
    x = (p + noise) / tau
    x = F.softmax(x.view(-1, p.size(1)))
    return x.view_as(p)



class CommunicationModule(nn.Module):

    def __init__(self, args):
        super(CommunicationModule, self).__init__()

        self.agents_no = agents_no = args["agents_no"]
        self.landmarks_no = landmarks_no = args["landmarks_no"]
        self.entities_no = entities_no = agents_no + landmarks_no
        self.vocabulary_size = vocabulary_size = args["vocabulary_size"]
        self.batch_size = batch_size = args["batch_size"]
        self.view_other_agents = view_other_agents = args["view_other_agents"]
        self.goal_agent_by_color = args["goal_agent_by_color"]
        self.use_communication = use_communication = args["use_communication"]
        self.use_cuda = args["use_cuda"]
        self.use_softmax_pool = use_softmax_pool = args["use_softmax_pool"]
        self.visible_no = entities_no if view_other_agents else (landmarks_no+1)

        self.color_size = 3
        self.goal_type_size = 3

        self.x_features_no = x_features_no = 2 + 2 + 2 + 3
        self.fc_out = fc_out = 256
        self.hidden_size = hidden_size = 32

        if self.goal_agent_by_color:
            self.g_size = g_size = 3 + 2 + 3
        else:
            self.g_size = g_size = 3 + 2 + 2

        self.fc_a_in = fc_a_in = fc_out * 2 + g_size

        self.actions_actv = actions_actv = 4
        self.communication_actv = communication_actv = vocabulary_size + 1

        #Model variables
        self.dropout_p = dropout_p = 0.1
        self.noise_hidden_states = lambda: 0.0

        self.fc_x = nn.Sequential(
            nn.Linear(x_features_no, fc_out),
            nn.ELU(),
            nn.Dropout(p=dropout_p),
            nn.Linear(fc_out, fc_out),
            nn.ELU(),
            nn.Dropout(p=dropout_p)
        )

        self.fc_c = nn.Sequential(
            nn.Linear(vocabulary_size + hidden_size, fc_out),
            nn.ELU(),
            nn.Dropout(p=dropout_p),
            nn.Linear(fc_out, fc_out + hidden_size),
            nn.ELU(),
            nn.Dropout(p=dropout_p)
        )

        self.fc_a = nn.Sequential(
            nn.Linear(fc_a_in + hidden_size, fc_out),
            nn.ELU(),
            nn.Dropout(p=dropout_p),
            nn.Linear(fc_out, fc_out + hidden_size),
            nn.ELU(),
            nn.Dropout(p=dropout_p)
        )

        self.fc_auxiliary_ag_color = nn.Sequential(
            nn.Linear(fc_out, self.color_size),
            nn.Sigmoid()
        )
        self.fc_auxiliary_lm_color = nn.Sequential(
            nn.Linear(fc_out, self.color_size),
            nn.Sigmoid()
        )
        self.fc_auxiliary_type = nn.Sequential(
            nn.Linear(fc_out, self.goal_type_size),
            nn.Softmax()
        )

        self.pooling = nn.AdaptiveAvgPool1d(1)

        self.actions_policy = nn.Sequential(
            nn.Linear(fc_out, actions_actv),
            nn.Tanh()
        )

        self.communication_policy = nn.Sequential(
            nn.Linear(fc_out, communication_actv)
        )

        if not use_softmax_pool:
            self.fc_x_conv = nn.Sequential(
                nn.Conv1d(self.visible_no, 1, 1),
                nn.ELU()
            )
            self.fc_c_conv = nn.Sequential(
                nn.Conv1d(self.agents_no, 1, 1),
                nn.ELU()
            )

    def forward(self, states, all_hidden=None, predict_goals=False):

        hidden_c, hidden_a = (None, None) if all_hidden is None else all_hidden

        agents_no = self.agents_no
        batch_size = self.batch_size
        vocabulary_size = self.vocabulary_size
        x_features_no = self.x_features_no
        visible_no = self.visible_no
        g_size = self.g_size
        fc_out = self.fc_out
        hidden_size = self.hidden_size
        use_softmax_pool = self.use_softmax_pool

        N = agents_no * batch_size
        c_in_size = vocabulary_size + hidden_size
        physical, goal, verbal = states

        assert verbal.size() == Size((N, agents_no, vocabulary_size))
        assert physical.size() == Size((N, visible_no, x_features_no))
        assert goal.size() == Size((N, g_size))

        if hidden_c is None:
            hidden_c = Variable(torch.zeros(N, agents_no, hidden_size))
            hidden_a = Variable(torch.zeros(N, hidden_size))
            if self.use_cuda:
                hidden_c = hidden_c.cuda()
                hidden_a = hidden_a.cuda()

        # Process physical
        physical = physical.view(N * visible_no, x_features_no)
        x_out = self.fc_x(physical)
        x_out = x_out.view(N, visible_no, fc_out)

        if use_softmax_pool:
            # SOFTMAX POOL
            x_out = x_out.contiguous().transpose(1,2)\
                .contiguous()\
                .view(N * fc_out, visible_no)

            features_x = F.softmax(x_out)
            features_x = features_x.view(N, fc_out, visible_no)
            features_x = self.pooling(features_x).view(N, fc_out)
            features_x = features_x.view(N, fc_out)
        else:
            features_x = self.fc_x_conv(x_out)
            features_x = features_x.view(N, fc_out)

        # Process communication
        c_input = torch.cat([verbal, hidden_c], 2)
        c_input = c_input.view(N * agents_no, c_in_size)
        c_out = self.fc_c(c_input)

        # Extract & process communication hidden states
        c_out, hidden_state_delta_c = c_out.split(fc_out, 1)
        hidden_c = F.tanh(hidden_c + hidden_state_delta_c +
                                 self.noise_hidden_states())

        #Auxilary task
        goal_agent_color = self.fc_auxiliary_ag_color(c_out)
        goal_landmark_color = self.fc_auxiliary_lm_color(c_out)
        goal_type = self.fc_auxiliary_type(c_out)
        auxiliary_goal = torch.cat([goal_type, goal_landmark_color,
                                    goal_agent_color], 1)

        c_out = c_out.contiguous().view(N, agents_no, fc_out)

        if use_softmax_pool:
            # SOFTMAX POOL
            c_out = c_out.contiguous()\
                .transpose(1,2)\
                .contiguous()\
                .view(N * fc_out, agents_no)
            features_c = F.softmax(c_out)
            features_c = features_c.view(N, fc_out, agents_no)
            features_c = self.pooling(features_c).view(N, fc_out)
        else:
            features_c = self.fc_c_conv(c_out)
            features_c = features_c.view(N, fc_out)


        a_input = [features_c, goal, features_x, hidden_a]
        a_input = torch.cat(a_input, 1)
        a_out = self.fc_a(a_input)
        a_out, hidden_state_delta_a = a_out.split(fc_out, 1)
        hidden_a = F.tanh(hidden_a + hidden_state_delta_a +
                                 self.noise_hidden_states())

        actions = self.actions_policy(a_out) + \
                  Variable(torch.randn(N, 4).type_as(a_out.data) / 100)
        messages = gumbel(self.communication_policy(a_out)) \
                   .narrow(1, 0, self.communication_actv-1) \
                   .contiguous()

        return actions, auxiliary_goal, messages, (hidden_c, hidden_a)

